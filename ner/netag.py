import sys,codecs, io, os
from subprocess import Popen, PIPE, STDOUT
sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))
from percepclassify import perceptronclassify
def main(argv):
	sys.stdin=codecs.getwriter('ISO-8859-15')(sys.stdin.buffer)
	sys.stdout=codecs.getwriter('ISO-8859-15')(sys.stdout.buffer)
	file=codecs.open('mynerreadfile.in','wb',encoding='ISO-8859-15')
	model=codecs.open(sys.argv[1],'r',encoding='ISO-8859-15')
	p = Popen(['cat'],stdin=PIPE,stdout=file, shell=True)
	#stream=io.TextIOWrapper(sys.stdin.buffer,encoding='ISO-8859-15')
	test=sys.stdin.read()
	p.stdin.write(test)
	out=p.communicate()[0]
	#for line in sys.stdin.readlines(): #stream.readlines(): 
	#	p.stdin.write(line)#.encode('ISO-8859-15')
	#file.write(out.decode('ISO-8859-15'))
	file.close()
	file=codecs.open('mynerreadfile.in','rb',encoding='ISO-8859-15')
	featurelist=[]
	for line in file:
		
		words=line.replace('\n','').split(' ')
		if '' in words:
			words.remove('')
		for i in range(len(words)):
			word=words[i].rsplit('/')
			w=word[0]
			s=word[1]
			if i==0:
				pw='MFW'
				ps='MFT'
				pn='MFN'
			else:
				pw=words[i-1].rsplit('/')[0]
				ps=words[i-1].rsplit('/')[1]
				pn=' '
			if i==len(words)-1:
				nw='MLW'
				ns='MLT'
			else:
				nw=words[i+1].rsplit('/')[0]
				ns=words[i+1].rsplit('/')[1]
			feature = 'w:' + w + ' s:' + s + ' pw:' +pw+ ' pn:' +pn + ' ns:' +ns + ' ps:' + ps + ' nw:' + nw 
			featurelist.append(feature)
		featurelist.append('\n')
	perceptronclassify(model,featurelist,'ner')


if __name__=="__main__":
	main(sys.argv[1:])
