import sys, ast, re,collections
from perceplearn import perceptronlearn
import codecs

def main(argv):
	file=codecs.open(sys.argv[1],'r',encoding='latin-1')
	featurelist=[]
	for line in file:
		words=line.replace('\n','').split(' ')
		if '' in words:
			words.remove('')
		for i in range(len(words)):
			word=words[i].rsplit('/')
			w=word[0]
			s=word[1]
			n=word[2]
			if i==0:
				pw='MFW'
				ps='MFT'
				pn='MFN'
			else:
				
				pw=words[i-1].rsplit('/')[0]
				ps=words[i-1].rsplit('/')[1]
				pn=words[i-1].rsplit('/')[2]
			if i==len(words)-1:
				nw='MLW'
				ns='MLT'
			else:
				nw=words[i+1].rsplit('/')[0]
				ns=words[i+1].rsplit('/')[1]
			#previous word,tag,entity, current word,tag,entity and next word,tag
			feature = n + ' w:' + w + ' s:' + s + ' pw:' +pw+ ' pn:' +pn + ' ns:' +ns + ' ps:' + ps + ' nw:' + nw 
			featurelist.append(feature)
	#	print(str(featurelist))
	#sys.exit(2)
	labels=findlabels(featurelist)
	w=dict()
	wavg=dict()
	for label in labels:
		w[label]=dict()
		wavg[label]=dict()
	for feature in featurelist:
		#print(featurelist)
		keys=feature.split(' ')[1:]
		#key=' '.join(key)
		for label in labels:
			wtvector=w[label]
			wtavgvector=wavg[label]
			for key in keys:
				if key not in wtvector.keys():
					wtvector[key]=0
					wtavgvector[key]=0
			w[label]=wtvector
			wavg[label]=wtavgvector
			#print(label+' '+str(w[label]))
		output=open(sys.argv[2],'w')
	perceptronlearn(output,w,wavg,labels,featurelist,'ner')
	
	
def findlabels(featurelist):
	labels=[]
	for feature in featurelist:
		labels.append(feature.split(' ')[0])
	return set(labels)

if __name__=="__main__":
	main(sys.argv[1:])
