Syntax:
----------------------------------------
----------------------------------------
python3 perceplearn.py input.txt model.nb
python3 percepclassify.py model.nb test.txt

python3 postrain.py train.pos model.pos
python3 postag.py model.pos test.pos

python3 nelearn.py train.ner model.ner
python3 netag.py model.ner test.ner
------------------------------------------
-------------------------------------------

Part-3 Answers
-----------------
-----------------
(a) Accuracy of POSTagger = 93.84

(b) Named Entity Recognition
Entity: ORG
Precision 0.820
Recall 0.637
FScore 0.717

Entity PER
Precision 0.865
Recall 0.577
FScore 0.692

Entity LOC
Precision 0.783
Recall 0.584
FScore 0.669

Entity MISC
Precision 0.604
Recall 0.445
FScore 0.512

Overall F Score =  0.917

(c) 
Dataset: SPAM/HAM
Naive Bayesian Classification results
F-Score (SPAM): 0.9673
Averaged Perceptron results
F-Score (SPAM) : 0.9754
Observation: It can be seen that the averaged perceptron works slightly better than Naive Bayesian
Reason:Naive Bayesian assumes feature independence. Also we iterate over the data only once. Averaged perceptron , on the other hand is a discriminative model which considers dependence among featured. By making multiple iterations for averaged perceptron, we ensure that the results close  to convergence. 
The results would differ more for POS and NER where avergaed perceptron will function much better due to the dpeendencies between close words and tags.
