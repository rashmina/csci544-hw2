import sys, ast,re,codecs
from subprocess import Popen, PIPE, STDOUT

def main(argv):
	model=open(sys.argv[1],'r')
	file=open('myperceptronreadfile.in','w')
	p = Popen(['cat'],stdin=PIPE,stdout=file)
	line=sys.stdin.read()
	p.stdin.write(line.encode())
	out=p.communicate()[0]
	#file.write(out.decode())
	file.close()
	file=open('myperceptronreadfile.in','r')
	featurelist=createfeatures(file)
	perceptronclassify(model,featurelist,'')

def createfeatures(file):
	w=dict()
	featurelist=[]
	for line in file:
		featurevector=line.replace('\n',' ').split(' ')[1:]
		featurevector.remove('')
		#print(linefeature)
		x = ['w:' + x for x in featurevector]
		linefeature= ' '.join(x)
		featurelist.append(linefeature)
		
	#print(featurelist)
	return featurelist


def perceptronclassify(model,featurelist,t):
	w=model.read()
	w=ast.literal_eval(w)
	z=dict()
	printline=''
	for line in featurelist:
		if t=='pos'  and line=='\n':
			sys.stdout.write(printline+'\n')
			sys.stdout.flush()
			printline=''
		if t=='ner' and line=='\n':
			#out=codecs.getwriter('latin-1')(sys.stdout.buffer)
			sys.stdout.write(printline+'\n')
			sys.stdout.flush()
			printline=''
		maxval=-2000
		maxlabel=''
		x=line.replace('\n',' ').split()
		if t=='pos':
			x=' '.join(x)
			pw=re.compile(r"(.+)(pw:)(.+)")
			m=pw.match(x)
			if m==None:
				continue
			pw=m.group(3).split(' ')[0]
			currentword=m.group(1).rstrip()[2:]
			if pw!='MW':
				x=m.group(1)+m.group(2)+''.join(m.group(3).split(' ')[0]) + ' ps:' + previousspeech + ' ' + ''.join(m.group(3).split(' ')[2])

			x=x.replace('\n',' ').split()

		if t=='ner':
			x=' '.join(x)
			#sys.stdout.write(x.decode('latin-1'))
			pw=re.compile(r"(.+)(pw:)(.+)")
			m=pw.match(x)
			if m==None:
				continue
			pw=m.group(3).split(' ')[0]
			currentword=m.group(1).rstrip()[2:]
			if pw!='MFW':
				x=m.group(1)+m.group(2)+''.join(m.group(3).split(' ')[0]) + ' pn:' + previousentity + ' ' + ''.join(m.group(3).split(' ')[2])
			x=x.replace('\n',' ').split()
	
		for label in w.keys():
			#print(label)
			wtvector=w[label]
			z[label]=0
			for key in x:
				if key in wtvector.keys():
					z[label]+=wtvector[key] 
		
			if z[label]>maxval:
				maxlabel=label
				maxval=z[label]
			#print(str(z[label]) + ' ' + label)
		
		if t=='pos':
			printline = printline + currentword+'/'+maxlabel + ' '
			previousspeech=maxlabel
		elif t=='ner':
			printline = printline + currentword.replace(' s:','/')+'/'+maxlabel + ' '
			previousentity=maxlabel
		else:
			print(maxlabel)
	
				
if __name__=="__main__":
	main(sys.argv[1:])
