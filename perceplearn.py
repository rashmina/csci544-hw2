import sys,random

def main(argv):
	lines=open(sys.argv[1]).readlines()
	random.shuffle(lines)
	open(sys.argv[1],'w').writelines(lines)
	file = open(sys.argv[1],'r')
	output=open(sys.argv[2],'w')
	##output.write(str(w))
	labels=findlabels(file)
	w, wavg,featurelist=createfeatures(file,labels)
	perceptronlearn(output,w,wavg,labels,featurelist,'')

def createfeatures(file,labels):
	w=dict()
	wavg=dict()
	featurelist=[]
	for label in labels:
		w[label]=dict()
		wavg[label]=dict()
	for line in file:
		featurevector=line.replace('\n',' ').split(' ')[1:]
		featurevector.remove('')
		y=line.split(None,1)[0]
		linefeature = y + ' ' 
		#print(linefeature)
		x = ['w:' + x for x in featurevector]
		linefeature= linefeature + (' '.join(x))
		#linefeature.append(x)
		##print(linefeature)
		featurelist.append(linefeature)
		for label in labels:
			wtvector=w[label]
			wtavgvector=wavg[label]
			for word in featurevector:
					key='w:'+word
					key=key.replace('\'','')
					if key not in wtvector.keys():
						wtvector[key]=0
						wtavgvector[key]=0
			w[label]=wtvector
			wavg[label]=wtavgvector
	#print(featurelist)
	return w,wavg, featurelist

def perceptronlearn(output,w,wavg,labels,featurelist,t):
	maxiter=20
	z=dict()
	N=len(featurelist)
	for i in range(maxiter):
		for line in featurelist:
			maxval=-2000
			maxlabel=''
			featurevector=line.replace('\n',' ').split(' ')[1:]
			y=line.split(None,1)[0]
			for label in labels:
				wtvector=w[label]
				z[label]=sum(wtvector[key] for key in featurevector)
				if z[label]>maxval:
					maxval=z[label]
					maxlabel=label
				##print(label + ' ' + str(z[label]))

			if maxlabel!=y:
				for label in labels:
					wtvector=w[label]
					for key in featurevector:
						if label==maxlabel:
							wtvector[key]-=1
						elif label==y:
							wtvector[key]+=1
					w[label]=wtvector
					
			
		for label in labels:
			wtvector=w[label]
			wtavgvector=wavg[label]
			for key in wtavgvector:
				wtavgvector[key]+=wtvector[key]
			wavg[label]=wtavgvector
		print(i)	
	
	wavgcopy=dict()
	for label in labels:
		wtavgvector=wavg[label]
		wavgcopy[label]=dict()
		wtavgvectorcopy=wavgcopy[label]
		for key,val in 	wtavgvector.items():
			wtavgvector[key]/=maxiter #removed maxiter*N
			if wtavgvector[key]!= 0:
				wtavgvectorcopy[key]=wtavgvector[key] 
		wavg[label]=wtavgvector
		wavgcopy[label]=wtavgvectorcopy

	output.write(str(wavgcopy))




def findlabels(file):
	labels=[]
	for line in file:
		labels.append(line.split(None,1)[0])
	file.seek(0)
	return set(labels)
	

if __name__=="__main__":
	main(sys.argv[1:])
