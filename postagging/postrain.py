import sys, ast, re,collections, os
sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))
from perceplearn import perceptronlearn

def main(argv):
	file=open(sys.argv[1],'r')
	featurelist=[]
	for line in file:
		words=line.replace('\n','').split(' ')
		if '' in words:
			words.remove('')
		for i in range(len(words)):
			word=words[i].rsplit('/')
			w=word[0]
			s=word[1]
			if i==0:
				pw='MW'
				ps='MT'
			else:
				
				pw=words[i-1].rsplit('/')[0]
				ps=words[i-1].rsplit('/')[1]
			if i==len(words)-1:
				nw='MW'
			else:
				nw=words[i+1].rsplit('/')[0]
			feature = s + ' w:' +w+ ' pw:' +pw+ ' ps:' +	ps + ' nw:' + nw 
			#feature = s + ' w:' +w+ ' pw:' +pw + ' nw:' + nw 
			featurelist.append(feature)
	#	print(str(featurelist))
	#sys.exit(2)
	labels=findlabels(featurelist)
	w=dict()
	wavg=dict()
	for label in labels:
		w[label]=dict()
		wavg[label]=dict()
	for feature in featurelist:
		#print(featurelist)
		keys=feature.split(' ')[1:]
		#key=' '.join(key)
		for label in labels:
			wtvector=w[label]
			wtavgvector=wavg[label]
			for key in keys:
				if key not in wtvector.keys():
					wtvector[key]=0
					wtavgvector[key]=0
			w[label]=wtvector
			wavg[label]=wtavgvector
			#print(label+' '+str(w[label]))
		output=open(sys.argv[2],'w')
	perceptronlearn(output,w,wavg,labels,featurelist,'pos')
	
	
def findlabels(featurelist):
	labels=[]
	for feature in featurelist:
		labels.append(feature.split(' ')[0])
	return set(labels)

if __name__=="__main__":
	main(sys.argv[1:])
