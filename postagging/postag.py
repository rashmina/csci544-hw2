import sys, os
from subprocess import Popen, PIPE, STDOUT
sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))
from percepclassify import perceptronclassify
def main(argv):
	model=open(sys.argv[1],'r')
	file=open('myposreadfile.in','w')
	p = Popen(['cat'],stdin=PIPE,stdout=file,shell=True)
	line=sys.stdin.read()
	p.stdin.write(line.encode())
	out=p.communicate()[0]
	file.close()
	file=open('myposreadfile.in','r')
	featurelist=[]
	for line in file:
		words=line.replace('\n','').split(' ')
		if '' in words:
			words.remove('')
		for i in range(len(words)):
			w=words[i]
			if i==0:
				pw='MW'
				ps='MT'
			else:
				
				pw=words[i-1]
				ps=' '
			if i==len(words)-1:
				nw='MW'
			else:
				nw=words[i+1]
			feature ='w:' +w+ ' pw:' +pw+ ' ps:' +	ps + ' nw:' + nw 
			featurelist.append(feature)
		featurelist.append('\n')
	perceptronclassify(model,featurelist,'pos')


if __name__=="__main__":
	main(sys.argv[1:])
